import { Input, message, Table, Modal, Button } from "antd";

import { useState } from "react";

import { useQuery, useMutation } from "urql";

import { PlusOutlined } from "@ant-design/icons";

export default function AdminComponent() {
  // States/refs
  const [username, setUsername] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState("");

  // Requests
  const GET_ADMINS = `
      query GET_ADMINS{
        getAdmins{
          id
          username
          password
          telephone
          removed
        }
      }
    `;

  const ADD_ADMIN = `
      mutation ADD_ADMIN(
        $username: String
        $password: String
        $telephone: String
      ){
        addAdmin(
          username:$username
          password: $password
          telephone: $telephone
        ){     
          id
          username
          password
          telephone
        }
      }
    `;

  const REMOVE_ADMIN = `
      mutation REMOVE_ADMIN(
        $id:ID!
      ){
        removeAdmin(id: $id){
          id
        }
      }
    `;

  const [{ data: aData, fetching: aFetching, error: aError }, reexeccuteQuery] =
    useQuery({
      query: GET_ADMINS,
    });

  const [addAdminResults, _addAdmin] = useMutation(ADD_ADMIN);
  const [removeAdminRes, _removeAdmin] = useMutation(REMOVE_ADMIN);

  if (!aData || aFetching) return <p>loading ...</p>;

  // Functions
  const handleCancel = (e) => {
    e.preventDefault();
    setModalVisible(false);
    setUsername(null);
    setPhoneNumber(null);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    let addAdmin = (payload) => {
      _addAdmin(payload)
        .then(({ data }) => {
          console.log(data?.addAdmin);
          message.success(
            `Successfully added admin with username '${payload?.username}'`
          );
          setModalVisible(false);
          setUsername(null);
          setPhoneNumber(null);
          reexeccuteQuery({ requestPolicy: "network-only" });
        })
        .catch((err) => {
          console.log(err);
          message.error("Oopps ..server error");
        });
    };

    const aPayload = {
      username,
      password: "westgatemart",
      telephone: phoneNumber,
    };

    console.log(aPayload);

    aData?.getAdmins.forEach((admin) => admin.username == aPayload.username)
      ?.length > 0
      ? message.info(`User with username '${aPayload.username} exists'`)
      : addAdmin(aPayload);
  };

  const removeAdmin = ({ id, username }) => {
    _removeAdmin({ id })
      .then(({ data }) => {
        if (data?.removeAdmin) {
          message.info(`User '${username}' removed.`);
          reexeccuteQuery({ requestPolicy: "network-only" });
          return;
        }
        message.warn("User not removed");
        return;
      })
      .catch((err) => message.error("Server error"));
  };

  const columns = [
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Phone number",
      dataIndex: "telephone",
      key: "telephone",
    },
    {
      title: "",
      dataIndex: "",
      render: ({ id, username }) => {
        return (
          <Button type="link" onClick={() => removeAdmin({ id, username })}>
            Remove
          </Button>
        );
      },
    },
  ];
  return (
    <div className="relative h-full">
      <div className="max-h-[90%]">
        <Table
          dataSource={aData?.getAdmins.filter((admin) => !admin?.removed)}
          columns={columns}
          size="small"
          scroll={{ y: 300 }}
          pagination={false}
        />
        <Modal
          visible={modalVisible}
          title="Add admin"
          onCancel={handleCancel}
          footer={null}
        >
          <form>
            <Input
              type="text"
              required
              value={username}
              placeholder="Username"
              onChange={(e) => setUsername(e.target.value)}
            />

            <Input
              type="tel"
              required
              style={{ margin: "16px 0px" }}
              value={phoneNumber}
              placeholder="Phone number"
              onChange={(e) => setPhoneNumber(e.target.value)}
            />

            <input
              className="bg-[#3F9B42] p-2 outline-none rounded-md w-full uppercase  text-gray-50 my-4"
              type="submit"
              value={"add"}
              onClick={handleSubmit}
            />
          </form>
        </Modal>
      </div>
      <Button
        onClick={() => setModalVisible(true)}
        type="primary"
        style={{
          borderRadius: "50%",
          marginTop: 12,
          float: "right",
          height: 45,
          width: 45,
        }}
      >
        <PlusOutlined />
      </Button>
    </div>
  );
}
