export default function Container({ children, style }) {
  return (
    <div style={{ ...style, padding: "16px 0px 16px 16px" }}>{children}</div>
  );
}
