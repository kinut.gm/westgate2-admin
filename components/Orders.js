import moment from "moment";

import {
  Empty,
  Avatar,
  Popconfirm,
  Tag,
  Menu,
  Typography,
  Tooltip,
  Dropdown,
  Button,
} from "antd";

import { useState } from "react";

import { useQuery, useMutation } from "urql";

import { FilterOutlined } from "@ant-design/icons";

const { Text } = Typography;

const AdminOrder = ({ data, filter }) => {
  // States
  const [confirmPacked, setConfirmPacked] = useState(false);
  const [popVisible, setPopVisible] = useState(false);
  const [confirmDelivered, setConfirmDelivered] = useState(false);
  const [popVisibleB, setPopVisibleB] = useState(false);

  // Requests
  const UPDATE_ORDER = `
      mutation UPDATE_ORDER($id:ID!, $packed: Boolean, $disbursed: Boolean , $status: String, $disburseDate:String){
        updateOrder(id: $id, packed: $packed, disbursed: $disbursed, status: $status, disburseDate: $disburseDate){
          id
        }
      }
    `;

  const [results, _updateOrder] = useMutation(UPDATE_ORDER);

  const onChangePacked = (id) => {
    setConfirmPacked(true);
    _updateOrder({
      id,
      packed: true,
    }).then((res) => {
      console.log(res);
      setConfirmPacked(false);
      reexeccuteQuery({ requestPolicy: "network-only" });
      let url = `https://web.whatsapp.com/send?phone=${
        "+254" + data.customer.phoneNumber.substring(1)
      }&text=${encodeURI(
        `Westgate shop, Juja \n\n Your order has been packed.`
      )}&app_absent=0`;
      window.open(url);
    });
  };

  const onChangeDelivered = (id) => {
    setConfirmDelivered(true);
    _updateOrder({
      id,
      disbursed: true,
      status: "old",
      disburseDate: Date.now().toString(),
    }).then((res) => {
      console.log(res);
      setConfirmDelivered(false);
      reexeccuteQuery({ requestPolicy: "network-only" });
      let url = `https://web.whatsapp.com/send?phone=${
        "+254" + data.customer.phoneNumber.substring(1)
      }&text=${encodeURI(
        `Westgate shop, Juja \n\n Your order has been delivered.\n Thank you for shopping with us.We hope you shop with us again.`
      )}&app_absent=0`;
      window.open(url);
    });
  };

  const getTotal = (arr) => {
    let tot = 0;
    if (arr.length == 0) {
      return tot;
    } else {
      arr.forEach((product) => {
        tot += product?.quantity * product?.meta?.price;
      });
      return tot;
    }
  };

  return (
    <div className="mb-2">
      <div className="grid grid-cols-7 border border-gray-300 p-3">
        <div className="col-span-2">
          <Avatar size={56}>{data.customer.displayName.charAt(0)}</Avatar>
        </div>
        <div className="col-span-5">
          <Text ellipsis style={{ display: "block", color: "#3F9B42" }}>
            {data.customer.displayName}
          </Text>
          <p style={{ fontSize: "0.8rem" }}>{data.customer.phoneNumber}</p>
          <p style={{ fontSize: "0.8rem", color: "#707070" }}>
            {data.customer.location}
          </p>
          <p>
            <span>
              <Tag color="green">{data.payment?.ref}</Tag> |{" "}
              <code>Ksh. {data.payment?.amount}</code>
            </span>
          </p>
          <p>
            Ordered :{" "}
            <span>
              {moment(new Date(parseInt(data.createdAt))).format(
                "Do MM/YY  @ h:mm a"
              )}
            </span>
          </p>
          {filter == "disbursed" && (
            <p>
              Disburse :{" "}
              <span>
                {moment(new Date(parseInt(data.disburseDate))).format(
                  "Do MM/YY  @ h:mm a"
                )}
              </span>
            </p>
          )}
        </div>
      </div>

      <div className="p-3 bg-gray-200">
        {filter == "awaiting packing" && (
          <>
            {data.products.map((product, i) => {
              return (
                <div
                  style={{
                    borderBottom: "#f1f1f1 1px dashed",
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "8px 0px",
                    width: "90%",
                  }}
                  key={product.id}
                >
                  <Tooltip title={product?.meta?.name}>
                    <Text ellipsis style={{ width: 120 }}>
                      {product?.meta?.name}
                    </Text>
                  </Tooltip>
                  <Tag color="green" style={{ height: 24, width: 24 }}>
                    {product.quantity}
                  </Tag>
                  <span>@</span>
                  <span>{product.meta.price}</span>
                  <span>{product.quantity * product.meta.price}</span>
                </div>
              );
            })}
            <p style={{ width: "100%", textAlign: "right", padding: 12 }}>
              Tot : <strong>Ksh. {getTotal(data.products)}</strong>
            </p>

            <Popconfirm
              visible={popVisible}
              title="Set this order as packed ? This action is irreversible"
              okButtonProps={{ loading: confirmPacked }}
              onConfirm={() => onChangePacked(data.id)}
              onCancel={() => setPopVisible(false)}
              okText="Yes, sure"
            >
              <Button onClick={() => setPopVisible(true)} type="primary" block>
                Mark as packed
              </Button>
            </Popconfirm>
          </>
        )}

        {filter == "packed" && (
          <>
            {data.deliver && <Tag color={"orange"}>To be delivered</Tag>}
            {data.products.map((product) => (
              <div
                style={{
                  borderBottom: "#f1f1f1 1px dashed",
                  display: "flex",
                  justifyContent: "space-between",
                  padding: "8px 0px",
                  width: "90%",
                }}
                key={product.id}
              >
                <Tooltip title={product?.meta?.name}>
                  <Text ellipsis style={{ width: 120 }}>
                    {product.meta.name}
                  </Text>
                </Tooltip>

                <Tag color="green" style={{ height: 24, width: 24 }}>
                  {product.quantity}
                </Tag>
                <span>@</span>
                <span>{product.meta.price}</span>
                <span>{product.quantity * product.meta.price}</span>
              </div>
            ))}
            <p style={{ width: "100%", textAlign: "right", padding: 12 }}>
              Tot : <strong>Ksh. {getTotal(data.products)}</strong>
            </p>
            {!data.deliver && (
              <Popconfirm
                visible={popVisibleB}
                title="Set this order as disbursed ? This action is irreversible"
                okButtonProps={{ loading: confirmDelivered }}
                onConfirm={() => onChangeDelivered(data.id)}
                onCancel={() => setPopVisibleB(false)}
                okText="Yes, sure"
              >
                <Button
                  onClick={() => setPopVisibleB(true)}
                  type="primary"
                  block
                >
                  Mark as disbursed
                </Button>
              </Popconfirm>
            )}
          </>
        )}

        {filter == "to deliver" && (
          <>
            {data.products.map((product) => (
              <div
                style={{
                  borderBottom: "#f1f1f1 1px dashed",
                  display: "flex",
                  justifyContent: "space-between",
                  padding: "8px 0px",
                  width: "90%",
                }}
                key={product.id}
              >
                <Tooltip title={product.meta.name}>
                  <Text ellipsis style={{ width: 120 }}>
                    {product.meta.name}
                  </Text>
                </Tooltip>
                <Tag color="green" style={{ height: 24, width: 24 }}>
                  {product.quantity}
                </Tag>
                <span>@</span>
                <span>{product.meta.price}</span>
                <span>{product.quantity * product.meta.price}</span>
              </div>
            ))}
            <p style={{ width: "100%", textAlign: "right", padding: 12 }}>
              Tot : <strong>Ksh. {getTotal(data.products)}</strong>
            </p>

            <Popconfirm
              visible={popVisibleB}
              title="Set this order as delivered ? This action is irreversible"
              okButtonProps={{ loading: confirmDelivered }}
              onConfirm={() => onChangeDelivered(data.id)}
              onCancel={() => setPopVisibleB(false)}
              okText="Yes, sure"
            >
              <Button onClick={() => setPopVisibleB(true)} type="primary" block>
                Mark as delivered
              </Button>
            </Popconfirm>
          </>
        )}

        {filter == "disbursed" && (
          <>
            {data.products.map((product) => (
              <div
                style={{
                  borderBottom: "#f1f1f1 1px dashed",
                  display: "flex",
                  justifyContent: "space-between",
                  padding: "8px 0px",
                  width: "90%",
                }}
                key={product.id}
              >
                <Tooltip title={product?.meta?.name}>
                  <Text ellipsis style={{ width: 120 }}>
                    {product?.meta?.name}
                  </Text>
                </Tooltip>

                <Tag color="green" style={{ height: 24, width: 24 }}>
                  {product?.quantity}
                </Tag>
                <span>@</span>
                <span>{product?.meta?.price}</span>
                <span>{product?.quantity * product?.meta?.price}</span>
              </div>
            ))}
            <p style={{ width: "100%", textAlign: "right", padding: 12 }}>
              Tot : <strong>Ksh. {getTotal(data.products)}</strong>
            </p>
          </>
        )}
      </div>
    </div>
  );
};

export default function OrdersComponent() {
  // States
  const [filter, setFilter] = useState("awaiting packing");

  // Requests
  const GET_ORDERS = `
    query GET_ORDERS{
      getOrders{
        id       
        status
        deliver
        disbursed
        packed 
        payment{
          ref
          timestamp
          amount
        }
        customer{
          id
          displayName
          phoneNumber
          location
        }
        products{
          id
          meta{
            id
            image
            name
            price
          }
          quantity
        } 
        disburseDate
        createdAt
        updatedAt
      }
    }
  `;

  const [{ data: oData, fetching: oFetching, error: oError }, reexeccuteQuery] =
    useQuery({
      query: GET_ORDERS,
    });

  // Functions
  const menuFilterOrders = (
    <Menu>
      <Menu.Item key="5">
        <a onClick={() => setFilter("awaiting packing")}>Awaiting packing</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">
        <a onClick={() => setFilter("packed")}>Packed</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="2">
        <a onClick={() => setFilter("to deliver")}>To deliver</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="4">
        <a onClick={() => setFilter("disbursed")}>History</a>
      </Menu.Item>
    </Menu>
  );

  if (oFetching) return <p>Loading ....</p>;
  if (oError) return <p>Oh no ... error</p>;

  return (
    <>
      <div className="flex ">
        <p className="py-3 px-1">
          Showing : <Tag color={"green"}>{filter.toUpperCase()}</Tag>
        </p>
        <Dropdown overlay={menuFilterOrders} trigger={["click"]}>
          <Button type="link" className="text-[#3F9B42]">
            <FilterOutlined />
          </Button>
        </Dropdown>
      </div>

      <div>
        {/* Awaiting packing */}
        {filter == "awaiting packing" && (
          <div className="m-8 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 justify-evenly">
            {oData?.getOrders.filter(
              (order) =>
                order.packed == false && order.status == "in_processing"
            ).length > 0 ? (
              oData?.getOrders
                .filter(
                  (order) =>
                    order.packed == false && order.status == "in_processing"
                )
                .map((order) => (
                  <AdminOrder key={order.id} data={order} filter={filter} />
                ))
            ) : (
              <Empty description="No orders yet" />
            )}
          </div>
        )}

        {/* Packed */}
        {filter == "packed" && (
          <div className="m-8 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 justify-evenly">
            {oData?.getOrders.filter(
              (order) => order.packed == true && order.status == "in_processing"
            ).length > 0 ? (
              oData?.getOrders
                .filter(
                  (order) =>
                    order.packed == true && order.status == "in_processing"
                )
                .map((order) => (
                  <AdminOrder key={order.id} data={order} filter={filter} />
                ))
            ) : (
              <div className="w-[100vw] items-center my-10">
                <Empty description="No orders yet" />
              </div>
            )}
          </div>
        )}

        {/* To deliver */}
        {filter == "to deliver" && (
          <div className="m-8 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 justify-evenly">
            {" "}
            {oData?.getOrders.filter(
              (order) =>
                order.packed && order.deliver && order.status == "in_processing"
            ).length > 0 ? (
              oData?.getOrders
                .filter(
                  (order) =>
                    order.packed &&
                    order.deliver &&
                    order.status == "in_processing"
                )
                .map((order) => (
                  <AdminOrder key={order.id} data={order} filter={filter} />
                ))
            ) : (
              <div className="w-[100vw] items-center my-10">
                <Empty description="No orders yet" />
              </div>
            )}
          </div>
        )}

        {/* Disbursed */}
        {filter == "disbursed" && (
          <div className="m-8 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 justify-evenly">
            {oData?.getOrders.filter(
              (order) => order.disbursed && order.status == "old"
            ).length > 0 ? (
              oData?.getOrders
                .filter((order) => order.disbursed && order.status == "old")
                .map((order) => (
                  <AdminOrder key={order.id} data={order} filter={filter} />
                ))
            ) : (
              <div className="w-[100vw] items-center my-10">
                <Empty description="No orders yet" />
              </div>
            )}
          </div>
        )}
      </div>
    </>
  );
}
