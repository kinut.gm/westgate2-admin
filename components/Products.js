import { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import { useMutation } from "urql";
import { client } from "../helpers/algolia-search.js";
import {
  InstantSearch,
  Stats,
  Configure,
  connectInfiniteHits,
  connectSearchBox,
} from "react-instantsearch-dom";
import {
  Input,
  Modal,
  Image,
  Form,
  InputNumber,
  DatePicker,
  Select,
  Menu,
  Popconfirm,
  Tooltip,
  Typography,
  Tag,
  Dropdown,
  Button,
  message,
} from "antd";
import { FaPlus } from "react-icons/fa";
import { DownOutlined } from "@ant-design/icons";
import Compressor from "compressorjs";

const { Search } = Input;
const { Paragraph } = Typography;
const { Option } = Select;
const { RangePicker } = DatePicker;

const Product = ({ hit, onClickOptions }) => {
  const router = useRouter();

  // Requests
  const UPDATE_PRODUCT = `
  mutation UPDATE_PRODUCT(
    $id: ID!
    $category: String
    $sale_end: String
    $sale_start: String
    $image: String
    $name: String
    $offer_price: Int      
    $price: Int
    $quantity: Int
    $outstocked: Boolean
    $removed: Boolean     
  ){
    updateProduct(
      id: $id
      category: $category
      sale_end: $sale_end
      sale_start: $sale_start
      image: $image
      name: $name
      offer_price: $offer_price
      price: $price
      quantity: $quantity
      outstocked: $outstocked
      removed : $removed
    ){
      id
      category
    }
  }
  `;

  const [updateProductRes, _updateProduct] = useMutation(UPDATE_PRODUCT);

  // Functions
  const handleDeleteProduct = () => {
    _updateProduct({ id: hit?.id, removed: true }).then(({ data }) => {
      if (data?.updateProduct) {
        message.success(`'${hit?.name}' removed!`);
        router.reload();
        return;
      }
    });
  };

  const menu = (
    <Menu>
      <Menu.Item key="0">
        <a
          onClick={() => {
            onClickOptions({
              action: "add_to_offers",
              hit,
            });
          }}
        >
          Add to offers
        </a>
      </Menu.Item>
      <Menu.Item key="3">
        <a
          onClick={() => {
            onClickOptions({
              action: "edit_product",
              hit,
            });
          }}
        >
          Edit product
        </a>
      </Menu.Item>
      <Menu.Item key="3">
        <a
          onClick={() => {
            onClickOptions({
              action: "change_stock_status",
              hit,
            });
          }}
        >
          Change stock status
        </a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="4">
        <Popconfirm
          title="Are you sure to delete this product?"
          onConfirm={handleDeleteProduct}
          okText="Yes"
          cancelText="No"
        >
          <a>Delete product</a>
        </Popconfirm>
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <div className="relative col-span-1 bg-gray-50 p-4">
        {Date.now() > parseInt(hit?.sale_start) &&
          Date.now() < parseInt(hit?.sale_end) && (
            <Tag color="green" className="absolute top-0 right-0 z-20">
              OFFER!
            </Tag>
          )}

        {hit?.outstocked && (
          <Tag color="red" className="absolute top-0 left-0 z-2">
            OUTSTOCKED
          </Tag>
        )}

        <Image
          src={hit?.image}
          height={80}
          width={80}
          className="object-cover"
        />
        <Tooltip color={"#3F9B42"} title={hit?.name}>
          <Paragraph
            ellipsis={{ rows: 1, expandable: false }}
            className=" text-sm p-0 m-0 block my-1 max-w-[135px]"
          >
            <strong className="text-[#3F9B42]">{hit?.name}</strong>
          </Paragraph>
        </Tooltip>
        <span>
          <div>
            <p
              className={
                Date.now() > parseInt(hit?.sale_start) &&
                Date.now() < parseInt(hit?.sale_end) &&
                "line-through text-sm text-gray-600 my-0"
              }
            >
              Ksh. {hit?.price}
            </p>
            {Date.now() > parseInt(hit?.sale_start) &&
              Date.now() < parseInt(hit?.sale_end) && (
                <p className="my-0">Ksh. {hit?.offer_price}</p>
              )}
          </div>

          <div className="items-center flex w-full">
            <Tag color="blue">{hit?.category.toUpperCase()}</Tag>
          </div>
        </span>
        <Dropdown
          overlay={menu}
          trigger={["click"]}
          className="w-full items-center"
        >
          <Button type="link" color="#707070">
            More <DownOutlined />
          </Button>
        </Dropdown>
      </div>
    </>
  );
};

const ProductsComponent = () => {
  // States & refs
  const router = useRouter();
  const [addModal, setAddModal] = useState(false);

  const [image, setImage] = useState();
  const [imageUrl, setImageURL] = useState();
  const [base64, setBase64] = useState("");
  const [uploadLoading, setLoadingUpload] = useState(false);

  const [productName, setProductName] = useState("");
  const [productPrice, setProductPrice] = useState();
  const [categoryAdd, setCategoryAdd] = useState("sodas");

  // Mutation states
  const [offersModal, setOffersModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [stockStatus, setStockModal] = useState(false);
  const [offer, setOffer] = useState({
    start: null,
    end: null,
  });
  const [offer_price, setOfferPrice] = useState(0);
  const [activeProduct, setActiveProduct] = useState(null);
  const [AB, setAB] = useState(false);
  const [OB, setOB] = useState(false);

  const [loadingEdit, setLoadingEdit] = useState(false);
  const [editedPrice, setEditedPrice] = useState(null);
  const [editedName, setEditedName] = useState(null);
  const [editedCategory, setEditedCategory] = useState(null);

  // Requests
  const ADD_PRODUCT = `
     mutation addProduct(
       $category: String
       $sale_end: String
       $sale_start: String
       $image: String!
       $name: String
       $offer_price: Int  
       $price: Int
       $quantity: Int
       
     ){
       addProduct(
         category: $category
         sale_end: $sale_end
         sale_start: $sale_start
         image: $image
         name: $name
         offer_price: $offer_price
         price: $price
         quantity: $quantity
       
       ){
         name
         id          
       }
     }
   `;
  const [mResults, _addProduct] = useMutation(ADD_PRODUCT);

  const UPDATE_PRODUCT = `
mutation UPDATE_PRODUCT(
  $id: ID!
  $category: String
  $sale_end: String
  $sale_start: String
  $image: String
  $name: String
  $offer_price: Int      
  $price: Int
  $quantity: Int
  $outstocked: Boolean
  $removed: Boolean     
){
  updateProduct(
    id: $id
    category: $category
    sale_end: $sale_end
    sale_start: $sale_start
    image: $image
    name: $name
    offer_price: $offer_price
    price: $price
    quantity: $quantity
    outstocked: $outstocked
    removed : $removed
  ){
    id
    category
  }
}
`;

  const [updateProductRes, _updateProduct] = useMutation(UPDATE_PRODUCT);

  // Functions
  useEffect(() => {
    if (image) {
      new Compressor(image, {
        quality: 0.6, // 0.6 can also be used, but its not recommended to go below.
        success: async (compressedResult) => {
          // compressedResult has the compressed file.
          // Use the compressed file to upload the images to your server.
          const _imgURL = URL.createObjectURL(compressedResult);
          setImageURL(_imgURL);
          setBase64(await blobToBase64(compressedResult));
        },
      });
    }
    return;
  }, [image]);

  function blobToBase64(blob) {
    return new Promise((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result);
      reader.readAsDataURL(blob);
    });
  }

  const handleOkAdd = () => {
    // Upload image
    if (!imageUrl) {
      message.info(`Missing image`);
      return;
    }
    if (!productName || !productPrice) {
      message.info("Missing key fields");
      return;
    }

    let payload = {
      category: categoryAdd,
      name: productName,
      price: productPrice,
      quantity: 1,
      image: base64,
    };

    console.log(payload);

    _addProduct(payload)
      .then(({ error }) => {
        if (!error) {
          message.success(`Added product : ${productName}`);
          setProductName("");
          setProductPrice(null);
          setImageURL(null);
          setBase64(null);
          setAddModal(false);
          router.reload();
          return;
        }
        message.error("Mutation failed");
        return;
      })
      .catch((err) => {
        message.error("Server error");
        return;
      });
  };

  const onImageChange = (e) => {
    setImage(e.target.files[0]);
  };

  const handleAvailable = () => {
    setAB(true);
    _updateProduct({
      id: activeProduct?.id,
      outstocked: false,
    }).then(({ data }) => {
      console.log(data);
      if (data?.updateProduct) {
        message.success(`Product now in stock!`);
        setAB(false);
        router.reload();
        return;
      }
      message.error(`Server error`);
      return;
    });
  };

  const handleOutstocked = () => {
    setOB(true);
    _updateProduct({
      id: activeProduct?.id,
      outstocked: true,
    }).then(({ data }) => {
      console.log(data);
      if (data?.updateProduct) {
        message.success(`Product now out of stock!`);
        setOB(false);
        router.reload();
        return;
      }
      message.error(`Server error`);
      return;
    });
  };

  const handleAddToOffers = () => {
    if (!offer.start || !offer.end || offer_price == 0 || !offer_price) {
      message.info(`Missing fields`);
      return;
    }

    let payload = {
      id: activeProduct?.id,
      sale_start: offer.start.toString(),
      sale_end: offer.end.toString(),
      offer_price,
    };

    _updateProduct(payload)
      .then(({ data }) => {
        if (data?.updateProduct) {
          message.success(`Product now on offer!`);
          router.reload();
          return;
        }
        message.info("Oh no...");
      })
      .catch((err) => {
        message.error(`Server error`);
      });
  };

  const handleEditProduct = () => {
    setLoadingEdit(true);
    let _product = {
      id: activeProduct?.id,
      name: editedName ? editedName : activeProduct?.name,
      price: editedPrice ? editedPrice : activeProduct?.price,
      category: editedCategory ? editedCategory : activeProduct?.category,
    };
    _updateProduct(_product).then(({ data }) => {
      if (data?.updateProduct) {
        message.success("Product updated");
        setLoadingEdit(false);
        router.reload();
        return;
      }
      message.error("Server error");
      return;
    });
  };

  // Components

  const SearchBox = ({ currentRefinement, isSearchStalled, refine }) => (
    <Search
      allowClear
      className="col-span-1 mb-3 max-w-[60%] md:max-w-[40%]"
      placeholder="Search product..."
      value={currentRefinement}
      onChange={(event) => refine(event.currentTarget.value)}
    />
  );

  const CustomSearchBox = connectSearchBox(SearchBox);

  const Hits = ({ hits, hasPrevious, refinePrevious, hasMore, refineNext }) => {
    const fetchMore = () => {
      if (
        scrollDiv.current.offsetHeight + scrollDiv.current.scrollTop >=
          scrollDiv.current.scrollHeight - 100 &&
        hasMore
      ) {
        refineNext();
      }
    };
    const scrollDiv = useRef();

    return (
      <div
        ref={scrollDiv}
        onScroll={fetchMore}
        style={{ maxHeight: "calc(100vh - 190px)", overflow: "scroll" }}
        className="relative gap-4 grid grid-cols-2 sm:grid-cols-4 md:grid-cols-5 lg:grid-cols-7 max-h-[calc(100vh - 180px)] overflow-y-auto"
      >
        {hits
          .filter((hit) => !hit.removed)
          .map((hit, i) => (
            <Product
              key={i}
              hit={hit}
              onClickOptions={(options) => {
                switch (options.action) {
                  case "add_to_offers":
                    setActiveProduct(options.hit);
                    setOffersModal(true);
                    break;
                  case "edit_product":
                    setActiveProduct(options.hit);
                    setEditModal(true);
                    break;
                  case "change_stock_status":
                    setActiveProduct(options.hit);
                    setStockModal(true);
                    break;
                  default:
                    break;
                }
              }}
            />
          ))}
      </div>
    );
  };

  const CustomHits = connectInfiniteHits(Hits);

  return (
    <InstantSearch searchClient={client} indexName="westgate-products">
      <div className="flex justify-between w-full">
        <div className="flex space-x-3 min-w-[60vw]">
          <CustomSearchBox />
        </div>

        <Stats />
      </div>

      <Configure hitsPerPage={70} analytics={false} />

      <CustomHits />

      <button
        onClick={() => setAddModal(true)}
        className=" z-20 bg-[#3F9B42] p-4 rounded-[50%] border border-gray-300 absolute right-4 bottom-4 "
      >
        <FaPlus className="text-white" />
      </button>

      <Modal
        title="Add product"
        visible={addModal}
        okText={uploadLoading ? "Uploading" : "Save"}
        onOk={handleOkAdd}
        onCancel={() => {
          setProductName("");
          setProductPrice(null);
          setImageURL(null);
          setBase64(null);
          setAddModal(false);
        }}
        confirmLoading={uploadLoading}
      >
        <Form>
          <Form.Item label="Product name">
            <Input
              value={productName}
              size="large"
              onChange={(e) => setProductName(e.target.value)}
              placeholder="Name"
            />
          </Form.Item>
          <Form.Item label="Price">
            <InputNumber
              size="large"
              style={{ width: "100%" }}
              onChange={(val) => setProductPrice(val)}
              addonBefore="KSH"
              placeholder="Price"
              value={productPrice}
            />
          </Form.Item>
          <Form.Item label="Category">
            <Select
              style={{ width: "100%" }}
              size="large"
              defaultValue="sodas"
              onChange={(category) => setCategoryAdd(category)}
            >
              {[
                "sodas",
                "juices",
                "medicine",
                "snacks",
                "legumes & flour",
                "breads & cakes",
                "spices & seasoning",
                "plastics",
                "milk products",
                "meat products",
                "frozen food",
                "electricals & electronics",
                "clothing",
                "toiletries",
                "soaps & cleaning",
                "dental products",
                "gas & gas products",
                "stationary",
                "fruits & vegetables",
                "oil & skin products",
                "hardware products",
                "other",
              ].map((category) => (
                <Option key={category}>{category.toUpperCase()}</Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Product Image">
            {image && imageUrl ? (
              <img
                src={imageUrl}
                style={{ width: "100%", objectFit: "cover" }}
              />
            ) : (
              <input type="file" accept="image/*" onChange={onImageChange} />
            )}
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        visible={stockStatus}
        onOk={null}
        footer={null}
        onCancel={() => {
          setStockModal(false);
          setActiveProduct(null);
        }}
        title="Change stock status"
      >
        <p>
          Current stock status of <strong>{activeProduct?.name}</strong>:{" "}
          <Tag color={activeProduct?.outstocked ? "red" : "green"}>
            {activeProduct?.outstocked ? "OUTSTOCKED" : "AVAILABLE"}
          </Tag>
        </p>
        <br />
        <div className="flex w-full justify-between">
          <Button
            loading={OB}
            type="primary"
            className="w-[45%]"
            onClick={handleOutstocked}
          >
            Set outstocked
          </Button>
          <Button
            loading={AB}
            type="dashed"
            className="w-[45%]"
            onClick={handleAvailable}
          >
            Set available
          </Button>
        </div>
      </Modal>

      <Modal
        title="Add to offers"
        visible={offersModal}
        onOk={handleAddToOffers}
        onCancel={() => {
          setOffersModal(false);
          setOffer({
            start: null,
            end: null,
          });
          setOfferPrice(0);
          setActiveProduct(null);
        }}
      >
        <div className="flex space-x-3 mb-4">
          <Image
            src={activeProduct?.image}
            height={56}
            width={56}
            className="object-cover"
          />
          <div>
            <Paragraph className=" text-sm p-0 m-0 block">
              <strong className="text-[#3F9B42]">{activeProduct?.name}</strong>
            </Paragraph>
            <Tag color="green">Ksh. {activeProduct?.price}</Tag>
          </div>
        </div>
        <RangePicker
          showTime
          className="w-full"
          use12Hours
          onChange={(a, b) => {
            if (!a) {
              setOffer({
                start: null,
                end: null,
              });
              return;
            }

            setOffer({
              start: new Date(a[0]).getTime(),
              end: new Date(a[1]).getTime(),
            });
          }}
        />
        <InputNumber
          style={{
            width: "100%",
            margin: "12px 0px",
          }}
          prefix="Ksh."
          value={offer_price}
          onChange={(val) => setOfferPrice(val)}
        />
      </Modal>

      <Modal
        title="Edit product"
        visible={editModal}
        onOk={handleEditProduct}
        onCancel={() => {
          setEditModal(false);
          setActiveProduct(null);
        }}
        okText={loadingEdit ? "Updating" : "Update"}
        confirmLoading={loadingEdit}
      >
        <Form>
          <Form.Item label="Product name">
            <Input
              value={editedName ? editedName : activeProduct?.name}
              onChange={(e) => setEditedName(e.target.value)}
              size="large"
            />
          </Form.Item>
          <Form.Item label="Category">
            <Select
              style={{ width: "100%" }}
              size="large"
              value={editedCategory}
              defaultValue={activeProduct?.category}
              onChange={(category) => setEditedCategory(category)}
            >
              {[
                "sodas",
                "juices",
                "medicine",
                "snacks",
                "legumes & flour",
                "breads & cakes",
                "spices & seasoning",
                "plastics",
                "milk products",
                "meat products",
                "frozen food",
                "electricals & electronics",
                "clothing",
                "toiletries",
                "soaps & cleaning",
                "dental products",
                "gas & gas products",
                "stationary",
                "fruits & vegetables",
                "oil & skin products",
                "hardware products",
                "other",
              ].map((category) => (
                <Option key={category}>{category.toUpperCase()}</Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Price">
            <InputNumber
              prefix="Ksh."
              value={editedPrice ? editedPrice : activeProduct?.price}
              style={{ width: "100%" }}
              size="large"
              onChange={(val) => setEditedPrice(val)}
            />
          </Form.Item>
        </Form>
      </Modal>
    </InstantSearch>
  );
};

export default ProductsComponent;
